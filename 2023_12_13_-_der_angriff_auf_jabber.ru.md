---
title: Der Angriff auf jabber.ru
theme: solarized
---

## Der Angriff auf jabber.ru
#### und was wir dagegen tun können
[Daniel Gultsch](https://gultsch.de)
<p><small>December 13th, 2023</small></p>

---

### Disclaimer

* Die angebliche Attacke¹ könnte komplett fingiert sein
  * Relativ detailierte und aufwendige Fälschung (tcpdumps + E-Mails an den Provider)
  * Unklares Ziel (der Fälschung)
* Wir wissen nicht wer es war

<small>¹: https://notes.valdikss.org.ru/jabber.ru-mitm/</small>
---

### Passive MITM

<img style="border: 0px; height: 450px; background-color: transparent;" src="images/jabber.ru/mitm_passive.png">

---

### Active MITM

<img style="border: 0px; height: 450px; background-color: transparent;" src="images/jabber.ru/mitm_active.png">

---

### Wie erkennen wir das?

* TCP/TLS Verbindung zwischen Client und MITM ≠ Verbindung MITM Server
  * TTL anders
  * Andere source ports
  * Anderes Client Hello (ciphers, random bytes)
* +1 hop für passive MITM ports (22, 5223)

---

### TLS certificates?

> Aber sollten TLS certificates nicht belegen das wir mit jabber.ru reden?

---

### TLS certificates!

* ACME challenge trivial abzufangen wenn man zwischen Letsencrypt Server und jabber.ru sitzt.
* MITM proxy stellt sich selber certificates aus…

---

### Aufgeflogen

* … bis der Praktikant vergisst das certificate zu verlängern

---

### CAA records

* DNS records die besagen das für die (sub) domain nur ein bestimmter ACME account certificates ausstellen kann
* Ggf zusätzlich auch Methode (DNS, HTTP)
* [snikket.org/blog/on-the-jabber-ru-mitm](https://snikket.org/blog/on-the-jabber-ru-mitm/)

---

### Certificate Transparency

* CA trägt jedes ausgestellte Certificate in ein öffentliches log ein
* Client prüft das das geschehen ist
* Server Betreiber durchsucht öffentliche logs nach seiner Domain

---

### Exkurs: SCRAM

* Standard Authentifizierung in XMPP²
* Client und Server beweisen sich gegenseitig das sie das Passwort kennen
* Passwort nicht übermittelt → Server nicht in der Lage sich als User auszugaben

<small>²: PLAIN existiert für seltene Einsätze</small>

---

### SCRAM-PLUS (Channel Binding)

* Passwort + etwas Eindeutiges aus der TLS Verbindung

---

### Channel Binding Methoden

* Unique (TLSv1.2)
* Exporter (TLSv1.3)
* Endpoint (certificate hash)³

<p><small>³: schützt nicht vor gestohlenen Certificates</small></p>

---

### XEP-0440

* SASL/SCRAM kann nur raten
* Welche Methoden kann mein Server

---

### Software Support

* Clients
  * Conversations + forks
  * Monal
* Server
  * Prosody (trunk)
  * ejabberd (trunk)

---

### Zusätzliche Hinweise

* `endpoint` ist so semi secure
* Passwort geheim halten!

---

# Danke!

<small>[gultsch.de](https://gultsch.de) · [daniel@gultsch.de](xmpp:daniel@gultsch.de) · [@daniel@gultsch.social](https://gultsch.social/@daniel)</small>

