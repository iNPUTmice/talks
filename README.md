A collection of talks
=====================

This repository contains an incomplete collection of talks [Daniel Gultsch](https://gultsch.de) gave at various conferences.

All slides are under the [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) ](https://creativecommons.org/licenses/by-sa/4.0/) license.


Display
-------

I use two different tools to create slides. Latex Beamer and [reveal-md](https://github.com/webpro/reveal-md). reveal-md slides have a .md file extension. You can display the slides with the tool [reveal-md](https://github.com/webpro/reveal-md) simply by running `reveal-md slides.md`. I’m also including pdf exports for those slides if you don’t want to install reveal-md. Latex Beamer slides natively export to PDF and are included in that file format only.


