---
title: Intro to Ltt.rs
theme: solarized
---

# Intro to Ltt.rs
### a JMAP e-mail client for Android
[Daniel Gultsch](https://gultsch.de)
<p><small>February 4th, 2024 @ FOSDEM</small></p>

---

<div style="display: flex">
<div style="flex: 2;">
<img style="border: 0px; height: 480px; background-color: transparent;" src="images/fosdem_2024/1.png">
</div>
<div style="flex: 3;">

## About me

* XMPP Developer
  * Council of the XSF
  * [Conversations.im](https://conversations.im)
* Side project: JMAP
  * Since ~2017 (pre RFC)
  * Java library + [Ltt.rs](https://ltt.rs)
</div>
</div>

---

<div style="display: flex">
<div style="flex: 2;">
<img style="border: 0px; height: 480px; background-color: transparent;" src="images/fosdem_2024/2.png">
</div>
<div style="flex: 3;">

## Why JMAP?
* No extensions (fewer?)
* Send *and* receive
* JSON parsers readily available
* No MIME parsing required
* Built-in Push Support
* See Ricardo’s (omitted) slides on how weird IMAP is

</div>
</div>

---

<div style="display: flex">
<div style="flex: 2;">
<img style="border: 0px; height: 480px; background-color: transparent;" src="images/fosdem_2024/3.png">
</div>
<div style="flex: 3;">

## Architecture
* Java 17
* Android Jetpack (Room, Navigation, WorkManager, …)
* Room implementation of generic storage interface of `jmap-mua`
* Playground for new APIs (Predictive Back, Material U)
</div>
</div>

---

<div style="display: flex">
<div style="flex: 2;">
<img style="border: 0px; height: 480px; background-color: transparent;" src="images/fosdem_2024/4.png">
</div>
<div style="flex: 3;">

## Offline Capabilities
* All queries cached (including searches)
* Users actions (move to mailbox, mark read, …) persisted using WorkManager
</div>
</div>

---

<div style="display: flex">
<div style="flex: 2;">
<img style="border: 0px; height: 480px; background-color: transparent;" src="images/fosdem_2024/5.png">
</div>
<div style="flex: 3;">

## (Unified)Push
* Foreground: WebSockets or EventSource
* Open Source WebPush
  * Firebase (requires VAPID)
  * UnifiedPush (multiple backends including Conversations)
</div>
</div>

---

<div style="display: flex">
<div style="flex: 2;">
<img style="border: 0px; height: 480px; background-color: transparent;" src="images/fosdem_2024/7.png">
</div>
<div style="flex: 3;">

## Autocrypt
* Native support; no plugin
* It just works! Lock icon in compose screen
* Key import during account setup (setup message)
* Server devs: Search arbitrary e-mail headers! 
</div>
</div>

---

<div style="display: flex">
<div style="flex: 2;">
<img style="border: 0px; height: 480px; background-color: transparent;" src="images/fosdem_2024/6.png">
</div>
<div style="flex: 3;">

## Thank You!
* JMAP library: [codeberg.org/inputmice/jmap](https://codeberg.org/inputmice/jmap)
* Android Client: [codeberg.org/iNPUTmice/lttrs-android](https://codeberg.org/iNPUTmice/lttrs-android)
* Mastodon: [@daniel@gultsch.social](https://gultsch.social/@daniel)
</div>
</div>
<small>Slides <a href="https://codeberg.org/inputmice/talks">codeberg.org/inputmice/talks</a></small>

